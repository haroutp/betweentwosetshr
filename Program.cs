﻿using System;
using System.Collections.Generic;

namespace BetweenTwoSets
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static int getTotalX(int[] a, int[] b){
            int ax = a[0];
            for (int i = 1; i < a.Length; i++)
            {
                ax = lcm(ax, a[i]);

            }

            int bx = b[0];
            for (int i = 1; i < b.Length; i++)
            {
                bx = gcd(bx, b[i]);
            }
            
            List<int> list = new List<int>();

            int temp = ax;

            while(temp <= bx){
                list.Add(temp);
                temp += ax;
            }

            int counter = 0;

            foreach (var item in list)
            {
                bool mul = true;
                foreach (var element in b)
                {
                  if(element % item != 0){
                      mul = false;
                      break;
                  }   
                }
                if(mul){
                    counter++;
                }
            }
            return counter;
        }
        

        public static int gcd(int x, int y){
            if(y == 0){
                return x;
            }
            return gcd(y, x % y);
        }

        public static int lcm(int x, int y){
            return (x * y) / gcd(x, y);
        }

    }
}
